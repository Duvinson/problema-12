/*Requerimientos: el usuario debe ingresar el tamaño de la matriz y los numeros que van en esta

 Garantiza:  que imprima la matriz y que verifique si esta matriz es un cuadro mágico */

#include <iostream>

using namespace std;

int main()
{
    int orden;
    int yes=1;
    cout << "Ingrese el orden de la matriz cuadrada: " << endl;
    cin >> orden;
    int matriz[orden][orden];//arreglo creado dependiendo del orden ingresado
    int n,suma_fila_referencia=0,suma_columna_referencia=0,suma_filas=0,suma_columnas=0,suma_diagonal=0,suma_diagonal_inversa=0;

    for (int i=0;i<orden;i++){//ciclo que va llenando cada elemento de la matriz de acuerdo al numero ingresado
        for (int j=0;j<orden;j++){
            cout << "ingrese un numero: " << endl;
            cin >> n;
            *(*(matriz+i)+j)=n;
        }
    }
    cout<<endl;
    cout << "La matriz: " << endl;

    for (int i=0;i<orden;i++){//ciclo para mostrar la matriz
        for (int j=0;j<orden;j++){
            if (*(*(matriz+i)+j)<10){
                cout <<" "<<*(*(matriz+i)+j)<<" ";
            }else
                cout << *(*(matriz+i)+j)<<" ";
        }cout<<endl;
    }cout<<endl;

    for (int i=0;i<1;i++){//ciclo que suma los elementos de la primera fila para tenerlos como referencia para una futura suma
        for (int j=0;j<orden;j++){
            suma_fila_referencia+=*(*(matriz+i)+j);
        }
    }
    for (int j=0;j<1;j++){//ciclo que suma los elementos de la primera columna para tenerlos como referencia para una futura suma
        for (int i=0;i<orden;i++){
            suma_columna_referencia+=*(*(matriz+i)+j);
        }
    }

    if (suma_columna_referencia!=suma_fila_referencia) yes=0;//condicion para ver si las sumas de la fila y columna de referencia son iguales
    else{
        for (int i=0;i<orden;i++){//ciclo para verificar que la suma de los elementos de cada fila de lo mismo a la referencia
            for (int j=0;j<orden;j++){
                suma_filas+=*(*(matriz+i)+j);
            }if (suma_filas!=suma_fila_referencia) yes=0;
            suma_filas=0;
        }
        for (int j=0;j<orden;j++){//ciclo para verificar que la suma de los elementos de cada columna de lo mismo a la referencia
            for (int i=0;i<orden;i++){
                suma_columnas+=*(*(matriz+i)+j);
            }if (suma_columnas!=suma_columna_referencia) yes=0;
            suma_columnas=0;
        }
        for (int i=0;i<orden;i++){//ciclo para verificar si la suma de los elementos de la diagonal de lo mismo que a la referncia
            suma_diagonal+=*(*(matriz+i)+i);
        }if (suma_diagonal!=suma_fila_referencia) yes=0;
        for (int i=0;i<orden;i++){//ciclo para verificar si la suma de los elementos de la diagonal inversa de lo mismo que a la referencia
            for(int j=orden-1;j>=0;j--){
                suma_diagonal_inversa+=*(*(matriz+i)+j);
            }if (suma_diagonal_inversa!=suma_fila_referencia) yes=0;
            suma_diagonal_inversa=0;
        }
    }
    if (yes==0) cout << "no es un cuadro magico" <<endl;
    else
        cout << "es un cuadro magico" << endl;
    return 0;
}
